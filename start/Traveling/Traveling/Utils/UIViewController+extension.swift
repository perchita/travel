//
//  UIViewController+extension.swift
//  Traveling
//
//  Created by Nadia Perchenkova on 11/21/20.
//

import UIKit

extension UIViewController {
    
    func showAlert(title: String, message: String) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        
        present(alert, animated: true, completion: nil)
    }
    
    func presentDetails(with country: Country) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let vc = storyboard.instantiateViewController(withIdentifier: "CountryDetailsViewController") as? CountryDetailsViewController
        else {
             return
        }

        vc.country = country
        show(vc, sender: nil)
    }
}
