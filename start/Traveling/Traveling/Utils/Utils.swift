//
//  Utils.swift
//  Traveling
//
//  Created by Nadia Perchenkova on 11/21/20.
//

import Foundation

func L10n(_ key: String) -> String {
    NSLocalizedString(key, comment: "")
}
