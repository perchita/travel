//
//  AuthHelper.swift
//  Traveling
//
//  Created by Nadia Perchenkova on 11/19/20.
//

import UIKit

class AuthHelper {
    
    static let shared = AuthHelper()

    private init() {}

    func logOut() {
        setupRootViewController(with: "SwitchAuthViewController")
    }

    func login() {
        setupRootViewController(with: "TabBarViewController")
    }

    private func setupRootViewController(with identifier: String) {
        guard
            let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene,
            let sceneDelegate = windowScene.delegate as? SceneDelegate
        else { return }
    
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(identifier: identifier)
        
        sceneDelegate.window?.rootViewController = viewController
    }
}
