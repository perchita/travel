//
//  File.swift
//  Traveling
//
//  Created by Nadia Perchenkova on 11/24/20.
//

import Foundation

public struct Country: Codable {

    let name: String
    let id: String?
    let language: String?
    let currency: String?
    let visa: String?
    let summary: String?
    let vacationType: [String : VacationType]?
    let latitude: Double
    let longitude: Double

    enum CodingKeys: String, CodingKey {
        
        case name
        case id
        case language
        case currency
        case visa
        case summary
        case vacationType
        case latitude
        case longitude
    }
}

struct VacationType: Codable {
    
    let description: String?
    let cities: [String]?
    
    enum CodingKeys: String, CodingKey {
        
        case description
        case cities
    }
}
