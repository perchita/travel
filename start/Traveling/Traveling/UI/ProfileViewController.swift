//
//  ProfileViewController.swift
//  Traveling
//
//  Created by Nadia Perchenkova on 11/19/20.
//

import UIKit
import FirebaseAuth

class ProfileViewController: UIViewController {
    
    //MARK: - Actions
    
    @IBAction func logOutButtonDidTap(_ sender: Any) {
        
        do {
            try Auth.auth().signOut()
        } catch {
            self.showAlert(title: L10n("logout.error"), message: "\(error.localizedDescription)")
        }
    }
}
