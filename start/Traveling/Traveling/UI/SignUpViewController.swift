//
//  SignUpViewController.swift
//  Traveling
//
//  Created by Nadia Perchenkova on 11/19/20.
//

import UIKit
import FirebaseAuth

class SignUpViewController: AuthViewController {
    
    //MARK: - Outlets
    
    @IBOutlet weak var signupButton: UIButton!
    
    //MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        signupButton.setTitle(L10n("signup"), for: .normal)
       
    }
    
    //MARK: - Actions
    
    @IBAction func signUpButtonDidTap(_ sender: Any) {
        
        guard
            let email = emailTextField.text,
            let password = passwordTextField.text,
            !email.isEmpty, !password.isEmpty
        else { return }
        
        if !password.isValidPassword {
            passwordErrorLable.text = L10n("invalid.password")
            passwordErrorLable.isHidden = false
            return
        }
        
        Auth.auth().createUser(withEmail: email, password: password) { [ weak self ] (result, error) in
            
            guard let self = self else { return }
            
            self.completeAuth(title: L10n("signup.error"), error: error)
        }
    }
}
