//
//  MapTabViewController.swift
//  Traveling
//
//  Created by Nadia Perchenkova on 11/25/20.
//

import UIKit
import MapKit
import FirebaseFirestore
import FirebaseFirestoreSwift

class MapTabViewController: UIViewController {
    
    // MARK: - Outlets

    @IBOutlet weak var mapView: MKMapView!
    
    // MARK: - Properties
    
    private let locationManager = CLLocationManager()
    var countries: [Country] = []
    var listner: ListenerRegistration?
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView.delegate = self
        mapView.showsUserLocation = true
        locationManager.delegate = self
        
        loadData()
        askLocationPermissions()
        
        let location = mapView.userLocation.location ?? CLLocation(latitude: 53.90000, longitude: 27.56667)
        mapView.centerToLocation(location)
    }
    
    private func askLocationPermissions() {
        locationManager.requestAlwaysAuthorization()
    }
    
    private func loadData() {
        
        let db = Firestore.firestore()
        listner = db.collection("countries").addSnapshotListener { [ weak self ](snapshot, error) in
            
            guard
                let self = self,
                error == nil,
                let documents = snapshot?.documents
            else { return }
            
            self.countries = []
            self.countries = documents.compactMap { document in
                return try? document.data(as: Country.self)
            }
            self.addAnnotations()
        }
    }
    
    private func addAnnotations() {
        
        var annotations: [MapAnnotation] = []
        
        for country in countries {
            let annotation = MapAnnotation(title: country.name,
                                           subtitle: nil,
                                           coordinate: CLLocationCoordinate2D(latitude: country.latitude, longitude: country.longitude))
            annotations.append(annotation)
        }
        mapView.addAnnotations(annotations)
    }
    
    deinit {
        listner?.remove()
    }
}

    // MARK: - MKMapViewDelegate

extension MapTabViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {

        guard let annotation = annotation as? MapAnnotation else { return nil }

        let identifier = "marker"
        var view: MKMarkerAnnotationView

        if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
            as? MKMarkerAnnotationView {
            dequeuedView.annotation = annotation
            view = dequeuedView
        } else {
            
            view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
        }
        return view
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        guard let annotation = view.annotation,
              let title = annotation.title
        else {
            return
        }
        
        for country in countries {
            if country.name == title {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                guard let vc = storyboard.instantiateViewController(withIdentifier: "ShortDetailsViewController") as? ShortDetailsViewController
                else {
                     return
                }
                
                vc.modalPresentationStyle = .overFullScreen
                vc.modalTransitionStyle = .crossDissolve
                vc.country = country
                vc.delegate = self
                
                present(vc, animated: true, completion: nil)
            }
        }
    }
}

    // MARK: - CLLocationManagerDelegate

extension MapTabViewController: CLLocationManagerDelegate {

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .authorizedAlways, .authorizedWhenInUse:

            manager.startUpdatingLocation()
            mapView.showsUserLocation = true
            
        default:
            break
        }
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        showAlert(title: L10n("error"), message: error.localizedDescription)
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let currentLocation = locations.last else { return }
    }
}

    // MARK: - MKMapView extension

private extension MKMapView {
    
    func centerToLocation(_ location: CLLocation, regionRadius: CLLocationDistance = 2000000) {
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate,
                                                  latitudinalMeters: regionRadius,
                                                  longitudinalMeters: regionRadius)
        setRegion(coordinateRegion, animated: true)
    }
}

    // MARK: - ShortDetailsViewControllerDelegate

extension MapTabViewController: ShortDetailsViewControllerDelegate {
    
    func shortDetailsViewController(_ viewController: ShortDetailsViewController, didTap moreButton: UIButton) {
        
        guard let country = viewController.country else { return }
        
        viewController.close {
            self.presentDetails(with: country)
        }
    }
}
