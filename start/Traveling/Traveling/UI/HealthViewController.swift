//
//  HealthViewController.swift
//  Traveling
//
//  Created by Nadia Perchenkova on 11/24/20.
//

import UIKit

class HealthViewController: BaseVacationTypeViewController {
    
    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        setUpView(for: "health")
    }
}
