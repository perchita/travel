//
//  SwitchAuthViewController.swift
//  Traveling
//
//  Created by Nadia Perchenkova on 11/19/20.
//

import UIKit



class SwitchAuthViewController: UIViewController {
    
    //MARK: - Outlets
    
    @IBOutlet weak var switchViewsConrtol: UISegmentedControl!
    @IBOutlet weak var logInView: UIView!
    @IBOutlet weak var signUpView: UIView!
    
    //MARK: - Properties
    
    private enum AuthOption: Int {
        case login
        case signUp
    }
    
    //MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        switchViewsConrtol.setTitle(L10n("login"), forSegmentAt: 0)
        switchViewsConrtol.setTitle(L10n("signup"), forSegmentAt: 1)
    
    }
    
    //MARK: - Actions
    
    @IBAction func switchViews(_ sender: UISegmentedControl) {
        
        switch AuthOption(rawValue: sender.selectedSegmentIndex) {
        case .login:
            logInView.alpha = 1
            signUpView.alpha = 0
        case .signUp:
            logInView.alpha = 0
            signUpView.alpha = 1
        default:
            break
        }
    }
}
