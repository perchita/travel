//
//  LoginViewController.swift
//  Traveling
//
//  Created by Nadia Perchenkova on 11/19/20.
//

import UIKit
import FirebaseAuth

class LoginViewController: AuthViewController {
    
    //MARK: - Outlets
    
    @IBOutlet weak var loginButton: UIButton!
    
    //MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        loginButton.setTitle(L10n("login"), for: .normal)
    }
    
    //MARK: - Actions

    @IBAction func loginButtonDidTap(_ sender: Any) {
        
        guard
            let email = emailTextField.text,
            let password = passwordTextField.text,
            !email.isEmpty, !password.isEmpty
        else { return }
        
        Auth.auth().signIn(withEmail: email, password: password) { [ weak self ] (result, error) in
            
            guard let self = self else { return }
            
            self.completeAuth(title: L10n("login.error"), error: error)
            
        }
    }
}
