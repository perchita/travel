//
//  CountryTableViewCell.swift
//  Traveling
//
//  Created by Nadia Perchenkova on 11/22/20.
//

import UIKit
import FirebaseFirestore
import FirebaseStorage
import FirebaseUI

class CountryTableViewCell: UITableViewCell {
    
    //MARK: - Outlets
    
    @IBOutlet weak var flagImageView: UIImageView!
    @IBOutlet weak var nameLable: UILabel!
    
    //MARK: - Properties
    
    static let cellID = "CountryTableViewCell"
    
    //MARK: - Lifecycle

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func update(with country: Country) {
        
        let name = country.name
        nameLable.text = name
        
        if let id = country.id{
            
            let storageRef = Storage.storage().reference()
            let reference = storageRef.child("flags/\(id).png")

            flagImageView.sd_setImage(with: reference, placeholderImage: nil) { (_, error, _, _) in
                if let error = error {
                    print(error)
                }
            }
        }
    }
}
