//
//  BaseVacationTypeViewController.swift
//  Traveling
//
//  Created by Nadia Perchenkova on 11/25/20.
//

import UIKit

class BaseVacationTypeViewController: UIViewController {
    
    // MARK: - Outlets
    
    @IBOutlet weak var discriptionLable: UILabel!
    @IBOutlet weak var citiesLable: UILabel!
    
    // MARK: - Properies
    
    var country: Country?
    
    // MARK: - Lifecycle

    func setUpView(for type: String) {
        
        guard let vacationTypes = country?.vacationType,
              let vacationType = vacationTypes[type],
              let cities = vacationType.cities
        else{
            return
        }
        
        var citiesList: String = ""
        
        for city in cities {
            citiesList += "• \(city)\n"
        }
        
        discriptionLable.text = vacationType.description
        citiesLable.text = citiesList
    }
}
