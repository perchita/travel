//
//  CountryDetailsViewController.swift
//  Traveling
//
//  Created by Nadia Perchenkova on 11/24/20.
//

import UIKit
import FirebaseStorage
import FirebaseUI

class CountryDetailsViewController: UIViewController {
    
    // MARK: - Outlets

    @IBOutlet weak var nameLable: UILabel!
    @IBOutlet weak var flagImageView: UIImageView!
    @IBOutlet weak var languageLable: UILabel!
    @IBOutlet weak var currencyLable: UILabel!
    @IBOutlet weak var visaLable: UILabel!
    @IBOutlet weak var summaryLable: UILabel!
    @IBOutlet weak var beachView: UIView!
    @IBOutlet weak var skiView: UIView!
    @IBOutlet weak var excursionView: UIView!
    @IBOutlet weak var healthView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var vacationTypeControl: UISegmentedControl!
    
    // MARK: - Properties
    
    var country: Country?
    var photos: [UIImage?] = []
    var containerViewController: BaseVacationTypeViewController?
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let country = country else { return }
        
        segmentedControlSettings()
        loadPhotos()
        
        if let id = country.id{
            
            let storageRef = Storage.storage().reference()
            let reference = storageRef.child("flags/\(id).png")

            flagImageView.sd_setImage(with: reference, placeholderImage: nil) { (_, error, _, _) in
                if let error = error {
                    print(error)
                }
            }
        }
        
        nameLable.text = country.name
        languageLable.text = country.language
        currencyLable.text = country.currency
        visaLable.text = country.visa
        summaryLable.text = country.summary
        
        collectionView.dataSource = self
        collectionView.delegate = self
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? BaseVacationTypeViewController {
            
            vc.country = country
        }
    }
    
    private func segmentedControlSettings() {
        
        if !isAvaliableVacationType("beach", segment: 0) {
            beachView.isHidden = true
        }
        
        if !isAvaliableVacationType("ski", segment: 1) {
            skiView.isHidden = true
        }
        
        if !isAvaliableVacationType("excursion", segment: 2) {
            excursionView.isHidden = true
        }
        
        if !isAvaliableVacationType("health", segment: 3) {
            healthView.isHidden = true
        }
    }
    
    private func isAvaliableVacationType(_ type: String, segment index: Int) -> Bool {
        guard let vacationTypes = country?.vacationType else { return false }

        var keys: [String] = []

        for i in 0..<vacationTypes.count  {
            let key = Array(vacationTypes.keys)[i] as String
            keys.append(key)
        }
        var isAvaliable = true
        if !keys.contains(type) {
            vacationTypeControl.setWidth(0.1 , forSegmentAt: index)
            vacationTypeControl.setEnabled(false, forSegmentAt: index)
            isAvaliable = false
        }
        return isAvaliable
    }
    
    private func loadPhotos() {
        
        guard let id = country?.id else { return }
        
        let storageRef = Storage.storage().reference()
        storageRef.child(id).listAll { list, error in
            
            guard error == nil else { return }
            
            for item in list.items {
    
                item.getData(maxSize: 1 * 1024 * 1024) { [ weak self] data, error in
                    guard let self = self,
                          error == nil,
                          let data = data
                    else {
                        return
                    }
                    
                    let photo = UIImage(data: data)
                    self.photos.append(photo)
                    self.collectionView.reloadData()
                }
            }
        }
    }
    
    // MARK: - Actions
    
    @IBAction func switchVacationTypes(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            beachView.alpha = 1
            skiView.alpha = 0
            excursionView.alpha = 0
            healthView.alpha = 0
        } else if sender.selectedSegmentIndex == 1 {
            beachView.alpha = 0
            skiView.alpha = 1
            excursionView.alpha = 0
            healthView.alpha = 0
        } else if sender.selectedSegmentIndex == 2 {
            beachView.alpha = 0
            skiView.alpha = 0
            excursionView.alpha = 1
            healthView.alpha = 0
        } else if sender.selectedSegmentIndex == 3 {
            beachView.alpha = 0
            skiView.alpha = 0
            excursionView.alpha = 0
            healthView.alpha = 1
        }
    }
}

    // MARK: - UICollectionViewDataSource

extension CountryDetailsViewController: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "photoCellID", for: indexPath) as? PhotoCollectionViewCell else {
            fatalError()
        }
        
        cell.photoImageView.image = photos[indexPath.row]
        
        return cell
    }
}

    // MARK: - UICollectionViewDelegate

extension CountryDetailsViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        collectionView.deselectItem(at: indexPath, animated: true)
        
        let photo = photos[indexPath.row]
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let vc = storyboard.instantiateViewController(withIdentifier: "PhotoViewController") as? PhotoViewController
        else {
             return
        }

        vc.photo = photo
//        present(vc, animated: true, completion: nil)
        show(vc, sender: nil)
    }
}

    // MARK: - UICollectionViewDelegateFlowLayout

extension CountryDetailsViewController: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let layout = collectionViewLayout as? UICollectionViewFlowLayout
        guard let spacing = layout?.minimumInteritemSpacing else { fatalError() }
        
        let numberOfCellsInRow: CGFloat = 3
        let fullWidth = view.frame.size.width - (2 * spacing)
        
        let width = fullWidth / numberOfCellsInRow
        
        return CGSize(width: width, height: width)
    }
}
