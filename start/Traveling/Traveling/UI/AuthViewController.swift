//
//  AuthViewController.swift
//  Traveling
//
//  Created by Nadia Perchenkova on 11/19/20.
//

import UIKit
import FirebaseAuth

class AuthViewController: UIViewController {
    
    //MARK: - Outlets

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailErrorLable: UILabel!
    @IBOutlet weak var passwordErrorLable: UILabel!
    
    //MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        emailTextField.placeholder = L10n("email")
        passwordTextField.placeholder = L10n("password")
        
        emailTextField.delegate = self
        passwordTextField.delegate = self
    }
    
    func completeAuth(title: String, error: Error?) {
        
        if let error = error as NSError? {
            
            guard let errorCode = AuthErrorCode(rawValue: error.code)
            else {
                self.showAlert(title: title, message: L10n("went.wrong"))
                return
            }
            
            switch errorCode {
            case .invalidEmail:
                self.showAlert(title: title, message: L10n("invalid.email"))
            case .wrongPassword:
                self.showAlert(title: title, message: L10n("wrong.password"))
            case .userNotFound:
                self.showAlert(title: title, message: L10n("user.not.found"))
            case .weakPassword:
                self.showAlert(title: title, message: L10n("invalid.password"))
            case .emailAlreadyInUse:
                self.showAlert(title: title, message: L10n("user.already.exists"))
            default:
                self.showAlert(title: title, message: L10n("went.wrong"))
            }
        }
    }
}

    //MARK: - UITextFieldDelegate

extension AuthViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == emailTextField {
            
            guard let email = emailTextField.text,
                  !email.isEmpty
            else {
                emailErrorLable.text = L10n("required")
                emailErrorLable.isHidden = false
                
                passwordTextField.becomeFirstResponder()
                return true
            }
            
            if !email.isValidEmail {
                emailErrorLable.text = L10n("invalid.email")
                emailErrorLable.isHidden = false
            }
            
            passwordTextField.becomeFirstResponder()
            
        } else if textField == passwordTextField {
            
            guard let password = passwordTextField.text,
                  !password.isEmpty
            else {
                passwordErrorLable.text = L10n("required")
                passwordErrorLable.isHidden = false
                self.view.endEditing(true)
                return true
            }
            
            if !password.isValidPassword {
                passwordErrorLable.text = L10n("invalid.password")
                passwordErrorLable.isHidden = false
            }
            
            self.view.endEditing(true)
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == emailTextField {
            emailErrorLable.isHidden = true
        } else if textField == passwordTextField {
            passwordErrorLable.isHidden = true
        }
    }
}
