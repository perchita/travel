//
//  ShortDetailsViewController.swift
//  Traveling
//
//  Created by Nadia Perchenkova on 11/25/20.
//

import UIKit

protocol ShortDetailsViewControllerDelegate: class {
    
    func shortDetailsViewController(_ viewController: ShortDetailsViewController, didTap moreButton: UIButton )
    
}


class ShortDetailsViewController: UIViewController {
    
    // MARK: - Outlets

    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var shortViewBottomConstaint: NSLayoutConstraint!
    @IBOutlet weak var shortViewTopConstaint: NSLayoutConstraint!
    @IBOutlet weak var visaLable: UILabel!
    @IBOutlet weak var currencyLable: UILabel!
    @IBOutlet weak var languageLable: UILabel!
    @IBOutlet weak var detailsView: UIView!
    @IBOutlet weak var nameLable: UILabel!
    
    // MARK: - Properties
    
    weak var delegate: ShortDetailsViewControllerDelegate?
    var country: Country?
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        languageLable.text = country?.language
        currencyLable.text = country?.currency
        visaLable.text = country?.visa
        nameLable.text = country?.name
        
        detailsView.layer.cornerRadius = 30
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(didTapBackground))
        backgroundView.addGestureRecognizer(tap)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        presentMenu(true) {}
    }
    
    public func close(completion: (() -> Void)?) {
        presentMenu(false, completionHandler: completion)
    }
    
    private func close() {
        presentMenu(false) { [weak self] in
            self?.dismiss(animated: true, completion: nil)
        }
    }
    
    private func presentMenu(_ present: Bool, completionHandler: ( () -> Void )? ) {
        let targetValue: UILayoutPriority = present ? .defaultHigh : .defaultLow
        UIView.animate(withDuration: 0.3, animations: {  [ unowned self ] in
            self.shortViewBottomConstaint.priority = targetValue
            self.view.layoutIfNeeded()
        }) { (completed) in
            if completed {
                completionHandler?()
            }
        }
    }
    
    @objc func didTapBackground() {
        close()
    }
    
    // MARK: - Actions
    
    @IBAction func closeButtonDidTap(_ sender: Any) {
        close()
    }
    
    @IBAction func moreButtonDidTap(_ sender: UIButton) {
        delegate?.shortDetailsViewController(self, didTap: sender)
    }
}
