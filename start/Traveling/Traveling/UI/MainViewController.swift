//
//  MainTabViewController.swift
//  Traveling
//
//  Created by Nadia Perchenkova on 11/22/20.
//

import UIKit
import FirebaseFirestore
import FirebaseFirestoreSwift
import FirebaseStorage

class MainViewController: UIViewController {
    
    // MARK: - Outlets

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Proteries
    
    var countries: [Country] = []
    var filteredCountries: [Country] = []
    var searching = false
    var listner: ListenerRegistration?
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nib = UINib(nibName: "CountryTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: CountryTableViewCell.cellID)

        tableView.dataSource = self
        tableView.delegate = self
        searchBar.delegate = self
        loadData()
    }
    
    private func loadData() {
        
        let db = Firestore.firestore()
        listner = db.collection("countries").addSnapshotListener { [ weak self ](snapshot, error) in
            
            guard
                let self = self,
                error == nil,
                let documents = snapshot?.documents
            else { return }
            
            self.countries = []
            self.countries = documents.compactMap { document in
                return try? document.data(as: Country.self)
            }
            self.countries = self.countries.sorted { $0.name < $1.name }
            self.tableView.reloadData()
            
        }
    }
    
    private func displayedCountries() -> [Country] {
        searching ? filteredCountries : countries
    }
    
    deinit {
        listner?.remove()
    }
}

    // MARK: - UITableViewDataSource

extension MainViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return displayedCountries().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellID = CountryTableViewCell.cellID
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as? CountryTableViewCell
        else {
            fatalError("Cell at indexPath: \(indexPath) with id: \(cellID)")
        }
        
        let country = displayedCountries()[indexPath.row]
        cell.update(with: country)
        
         return cell
    }
}

    // MARK: - UITableViewDelegate

extension MainViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        tableView.deselectRow(at: indexPath, animated: true)
        
        self.presentDetails(with: displayedCountries()[indexPath.row])
    }
}

    // MARK: - UISearchBarDelegate

extension MainViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        filteredCountries = countries.filter { $0.name.hasPrefix(searchText) }
        
        searching = true
        tableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        searching = false
        searchBar.resignFirstResponder()
    }
}
