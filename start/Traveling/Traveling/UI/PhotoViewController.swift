//
//  PhotoViewController.swift
//  Traveling
//
//  Created by Nadia Perchenkova on 11/26/20.
//

import UIKit

class PhotoViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    
    var photo: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageView.image = photo
    }
}
