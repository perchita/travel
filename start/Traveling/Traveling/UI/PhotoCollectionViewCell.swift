//
//  PhotoCollectionViewCell.swift
//  Traveling
//
//  Created by Nadia Perchenkova on 11/25/20.
//

import UIKit

class PhotoCollectionViewCell: UICollectionViewCell {
    
    // MARK: - Outlets
    
    @IBOutlet weak var photoImageView: UIImageView!
}
