//
//  BeachViewController.swift
//  Traveling
//
//  Created by Nadia Perchenkova on 11/24/20.
//

import UIKit

class BeachViewController: BaseVacationTypeViewController {
    
    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        setUpView(for: "beach")
    }
}
